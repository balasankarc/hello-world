all: compile

compile:
	$(CC) -o hello hello.c

install: compile
	install -D hello $(DESTDIR)/usr/bin/hello

clean:
	rm -f hello

uninstall:
	rm -f $(DESTDIR)/usr/bin/hello
